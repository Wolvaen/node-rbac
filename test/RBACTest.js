/**
 * Created by andre on 30.05.16.
 */
var chai = require('chai'),
    expect = chai.expect,
    RBAC = require('../index.js')();

function UserMock() {}
UserMock.prototype.roles = null;

describe("RBAC", function() {
    var user = new UserMock(),
        ROLES = {
            SUPERADMIN: {
                ADMIN: {
                    PREMIUMUSER: {
                        USER: {}
                    }
                }
            },
            SUPERVISOR: {
                ADMIN: {}
            },
            USER: {
                GUEST: {}
            }
        };
    before(function() {});
    describe("#setRoles", function() {
        RBAC.setRoles(ROLES);
        it("should set and normalize a new ROLE object", function() {
            expect(RBAC.constructor.ROLES).to.equal(ROLES);
            expect(RBAC.constructor._ROLES).to.contain.all.keys(Object.keys(ROLES));
            expect(RBAC.constructor._ROLES).to.not.equal(ROLES);
        });
    });
    describe("#getRoles", function() {
        it("should return our raw ROLE object", function() {
            expect(RBAC.getRoles()).to.equal(ROLES);
        });
    });
    describe("#checkPermission", function() {
        var permission = "ADMIN";
        it("should return true on SUPERADMIN", function() {
            user.roles = ["SUPERADMIN"];
            expect(RBAC.checkPermission(permission, user)).to.be.true;
        });
        it("should return true on [\"ADMIN\"] and [\"SUPERADMIN\", \"ADMIN\", \"USER\"]", function() {
            user.roles = ["ADMIN"];
            expect(RBAC.checkPermission(permission, user)).to.be.true;

            user.roles = ["SUPERADMIN", "ADMIN", "USER"];
            expect(RBAC.checkPermission(permission, user)).to.be.true;
        });
        it("should return false on [\"USER\", \"GUEST\"]", function() {
            user.roles = ["USER", "GUEST"];
            expect(RBAC.checkPermission(permission, user)).to.be.false;
        });
        it("should return false on invalid roles", function() {
            user.roles = null;
            expect(RBAC.checkPermission(permission, user)).to.be.false;
        });
    });
});