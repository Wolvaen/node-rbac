
var gulp = require('gulp'),
    mocha = require('gulp-mocha'),

    CONFIG = {};

CONFIG.MAIN = 'index.js';
CONFIG.LIB = 'lib';
CONFIG.TEST = 'test';

gulp.task('test', function() {
    return gulp.src(CONFIG.TEST+"/**/*.js", {read: false})
        .pipe(mocha({reporter: 'nyan'}));
});

gulp.task('default', ['test']);