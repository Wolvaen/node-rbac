# Simple hierarchical role based access control

## Usage

```js

var RBAC = require('../index.js')();

// Sets the hierarchical roles

RBAC.setRoles({

    // Default roles
    SUPERADMIN: {
        ADMIN: {
            PREMIUMUSER: {
                USER: {}
            }
        }
    },
    
    // Example for additional special permissions
    SUPERVISOR: {
        ADMIN: {}
    },
    USER: {
        GUEST: {}
    }

});

var user = new (function() {
    this.role = "USER";
})(); 

// Checks the permission for a user object with a role-attribute against the Role "ADMIN"
RBAC.checkPermission("ADMIN", user) // false

user.role = "SUPERADMIN";
RBAC.checkPermission("ADMIN", user) // true

user.role = "ADMIN";
RBAC.checkPermission("ADMIN", user) // true
    
```