/**
 * Created by andre on 30.05.16.
 */
"use strict";

function RBAC() {}

RBAC.singleton = null;
RBAC.ROLES = null;
RBAC._ROLES = null;

RBAC.prototype.setRoles = function(roles) {
    RBAC.ROLES = roles;
    RBAC._ROLES = {};
    this.normalizeRoles(roles);
};

RBAC.prototype.getRoles = function() {
   return RBAC.ROLES;
};

RBAC.prototype.normalizeRoles = function(roles, parent) {
    var that = this;
    Object.keys(roles).forEach(function(key) {
        if (!RBAC._ROLES.hasOwnProperty(key)) { RBAC._ROLES[key] = {}; }
        if (parent && !(parent in RBAC._ROLES[key])) { RBAC._ROLES[key][parent] = {} }
        that.normalizeRoles(roles[key], key);
    });
};

RBAC.prototype.checkPermission = function(permission, user) {
    var role, index, userRoles = user.roles;
    for (index in userRoles) {
        role = userRoles[index];
        if (RBAC._ROLES.hasOwnProperty(permission) && (role === permission || role in RBAC._ROLES[permission])) {
            return true;
        }
    }
    return false;
};

module.exports = function() {
    if (!RBAC.singleton) {
        return RBAC.singleton = new RBAC;
    }
    return RBAC.singleton;
};